#include "Nineball.h"

Nineball::Nineball()
{

}

Nineball::Nineball(bool log_type) : Game(log_type)
{
	//Init
	_balls.clear();
	_play_balls_start_amount = 10;

	//Create balls
	std::string topic_white = "MultiMediaBilliardTable\\ball\\Cue\\pos_";
	Ball *white = new Ball(MQTT_HOST, 1883, topic_white, 0, _eventPublisher, "Ball");	//create white ball
	_balls.push_back(white);
	white->connectToBroker();
	for (size_t i = 1; i < _play_balls_start_amount; i++)
	{
		std::string topic = "MultiMediaBilliardTable\\ball\\" + std::to_string(i) + "\\pos_";
		Ball *b           = new Ball(MQTT_HOST, 1883, topic, i, _eventPublisher, "Ball");

		_balls.push_back(b) ;
		b->connectToBroker();
	}

	//Select target ball
	_target_ball = _balls.begin();
	++_target_ball;

	//Show startup text on Table
	_eventPublisher->animation_text("Starting Nineball");
	
	 _prev_nine_game = new Nineball( *this );
	*_prev_nine_game = *this;

	run();
}

Nineball::~Nineball()
{
	delete _prev_nine_game;
}

Nineball::Nineball (const Nineball &orig) : Game (orig)
{
	_winning_ball          = orig._winning_ball;
	_winning_ball_pocketed = orig._winning_ball_pocketed;
	_target_ball           = _balls.begin() + 1;
}

Nineball & Nineball::operator= (const Nineball &orig)
{
	Game::operator= (orig);
	_winning_ball          = orig._winning_ball;
	_winning_ball_pocketed = orig._winning_ball_pocketed;
	_target_ball           = _balls.begin() + 1;

	return *this;
}

void Nineball::set_ball_start_positions()
{
	//Determine the balls for the random starting positions
	std::vector<std::string> randomBalls;
	randomBalls.push_back("2");
	randomBalls.push_back("3");
	randomBalls.push_back("4");
	randomBalls.push_back("5");
	randomBalls.push_back("6");
	randomBalls.push_back("7");
	randomBalls.push_back("8");
	std::random_shuffle(randomBalls.begin(), randomBalls.end());

	//Publish the starting ball positions according to initialBallPositions.png
	_eventPublisher->animation_startPositionBallNumber("1", "1");
	_eventPublisher->animation_startPositionBallNumber("2", randomBalls.front());
	randomBalls.erase(randomBalls.begin());
	_eventPublisher->animation_startPositionBallNumber("3", randomBalls.front());
	randomBalls.erase(randomBalls.begin());
	_eventPublisher->animation_startPositionBallNumber("4", randomBalls.front());
	randomBalls.erase(randomBalls.begin());
	_eventPublisher->animation_startPositionBallNumber("5", "9");
	_eventPublisher->animation_startPositionBallNumber("6", randomBalls.front());
	randomBalls.erase(randomBalls.begin());
	_eventPublisher->animation_startPositionBallNumber("7", "X");
	_eventPublisher->animation_startPositionBallNumber("8", randomBalls.front());
	randomBalls.erase(randomBalls.begin());
	_eventPublisher->animation_startPositionBallNumber("9", randomBalls.front());
	randomBalls.erase(randomBalls.begin());
	_eventPublisher->animation_startPositionBallNumber("10", "X");
	_eventPublisher->animation_startPositionBallNumber("11", "X");
	_eventPublisher->animation_startPositionBallNumber("12", "X");
	_eventPublisher->animation_startPositionBallNumber("13", randomBalls.front());
	randomBalls.erase(randomBalls.begin());
	_eventPublisher->animation_startPositionBallNumber("14", "X");
	_eventPublisher->animation_startPositionBallNumber("15", "X");
}

void Nineball::spot_nineball()
{
	std::string topic = "MultiMediaBilliardTable\\ball\\" + std::to_string(_winning_ball) + "\\pos_";
	Ball *b = new Ball(MQTT_HOST, 1883, topic, _winning_ball, _eventPublisher, "Ball");

	_balls.push_back(b);
	b->connectToBroker();
}

//TODO
void Nineball::evaluate_collisions()
{
	for (auto const& value : _detected_collision)
	{
		// if one was play_ball

		if (_first_turn)
		{
			if (_first_collision_of_turn)
			{
				_first_collision_of_turn = false;
				if (value[0]->get_subNumber() +  value[1]->get_subNumber() != 1)
				{
					_end_of_turn_events.push_back(FOUL);
					_foul_occurred_this_turn = true;
				}
			}
		}
		else
		{

		}
		if (value[0]->get_subNumber() == 0 || value[1]->get_subNumber() == 0)
		{

		}
	}
}

void Nineball::evaluate_pocketedBalls()
{
	//Were any balls pocketed at all this round?
	if (_balls_pocketed_this_turn.empty())
	{
		return;
	}

	for (auto it = _balls_pocketed_this_turn.begin(); it < _balls_pocketed_this_turn.end(); ++it) 
	{
		_ball_legally_pocketed = true;
		if (*it == 0)	//Cue ball pocketed
		{
			_foul_occurred_this_turn = true;
			continue; 
		}

		if (*it == _winning_ball)	//Winning ball pocketed
		{
			_winning_ball_pocketed = true;
			if ((*_target_ball)->get_subNumber() != _winning_ball)
			{
				_foul_occurred_this_turn = true;
			}
		} 
			
		for (std::vector<Ball*>::iterator ball_it = _balls.begin(); ball_it != _balls.end(); )
		{
			if (*it == (*ball_it)->get_subNumber())
			{
				if ((*_target_ball)->get_subNumber() == (*ball_it)->get_subNumber())
				{
					delete *ball_it;
					_target_ball = _balls.erase(_target_ball);
					ball_it = _target_ball;
				}
				else
				{
					delete *ball_it;
					ball_it = _balls.erase(ball_it);
				}
			}
			else
			{
				++ball_it;
			}
		}
	}
    _balls_pocketed_this_turn.clear();
}

void Nineball::change_turn()
{
	_incoming_team = (_incoming_team + 1) % _amount_of_teams;
	std::string currentTeamName = _teams.at(_incoming_team)->get_team_name();
	_eventPublisher->animation_text(currentTeamName + ", it's your turn now!");
	Logger::log(DEBUG, "Turn Changes \n");

}

void Nineball::new_turn()
{
	//Reset variables for new turn
	_balls_pocketed_this_turn.clear();
	_ball_hit_border         = 0;     //top_C/
	_foul_occurred_this_turn = false;
	_turns_team++;
	_ball_legally_pocketed   = false;
	_allow_start_of_turn     = false; //top_C/
	_first_collision_of_turn =  true; //top_C/
	*_prev_nine_game = *this;
}

void Nineball::win_and_clean_up()
{
	Logger::log(RELEASE, " The Game has finished \n");
	std::string currentTeamName = _teams.at(_incoming_team)->get_team_name();
	Logger::log(RELEASE, currentTeamName + " has pocketed the Nineball and won \n");

	_eventPublisher->animation_text_gameover(currentTeamName + " has pocketed the Nineball and won");
	_eventPublisher->game_won(currentTeamName);
}



void Nineball::run()
{
	check_for_updatedBalls ();
	evaluate_collisions ();

	check_pocketedBalls ();
	evaluate_pocketedBalls ();

	validate_ball_positions ();
	validate_play_ball_position ();

	//_visualisation->animation_ball(_balls.at(0), 0);
	
	//std::cout << " ALLOWED TO START TURN " << _allow_start_of_turn << "\n";
	// if nine ball has been holed correctly
	if (_game_finished)
	{
		std::cout << "GAME WON \n";		//TODO MQTT
		return;
	}

	if (_game_idle)
	{
		// if turns were changing and the play is resumed
		if (_ticks_since_last_ball_movement <= _ticks_until_idle)
		{
			if (_allow_start_of_turn)
			{
				_game_idle = false;
				_free_foul_ball_placement = false;
			}
			else
			{
				
			}
		}	
	}
	else
	{
		// if the current play is still ongoing and the balls are still moving
		if (_ticks_since_last_ball_movement <= _ticks_until_idle)
		{
			std::cout << "running  idle : " << _ticks_since_last_ball_movement << "\n";
		}
		// if the current play was still ongoing and now the balls have been idle enough for an end of the turn to be declared
		else
		{
			_game_idle = true;

			if (_first_turn)
			{
				if (!_ball_legally_pocketed)
				{
					for (auto const& value : _balls)
					{
						if (value->get_border_touched_this_turn()) _ball_hit_border++;
					}
					if (_ball_hit_border <= 4)	//Did at least 4 balls hit the border?
					{
						_foul_occurred_this_turn = true;
					}
				}
				_first_turn = false;
			}
			
			if (_foul_occurred_this_turn)
			{
				_eventPublisher->animation_text_foul("Foul!"); //display text on table
				if (_winning_ball_pocketed)
				{
					(*_balls.end())->set_start_x_and_y(1.1, 0);		//Reset winning ball position
				}
				change_turn();
			} 
			else
			{
				if (_winning_ball_pocketed)
				{
					_game_finished = true;	//Game over
				}
			}
			new_turn();
		}
	}
	_timer++;
	_ticks_since_last_ball_movement++;
	_sleep(_sleep_time_per_run_iteration);
}


