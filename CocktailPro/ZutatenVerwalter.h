//@(#) ZutatenVerwalter.h

#ifndef ZUTATENVERWALTER_H_H
#define ZUTATENVERWALTER_H_H

#include <string>
#include <map>
#include <vector>
#include <list>
#include <fstream>
#include <cstdlib> // fuer exit() unter Linux
#include <iostream>

#include "Zutat.h"
/**
 *  Liest Zutaten aus Liste ein.
 *  Merkt sich in der Liste die Zutaten + Aggregatszustand.
 *
 */
 
 
 /**	 
  * @class ZutatenVerwalter
  *
  * @brief Reads the ingredients from a list.
  * Saves these ingredients with the corresponding state in the list.
  *
  */
class ZutatenVerwalter {
public:


	/**
	 * @brief Constructor generating a new vector of ingredients
	 *
	 * reads the ingriedients with a private method.
	 * Sets the maximum number of the "Dosierer" equal to the read ingredients.
	 */
	 
    ZutatenVerwalter(void);
    
    /**
     * @brief copy constructor
     * 
     * @param orig the copied ZutatenVerwalter
     */
    
    ZutatenVerwalter(const ZutatenVerwalter& orig);
    
    virtual ~ZutatenVerwalter();

	
	/**
	 * @brief Displays the available ingredients 
	 *
	 * iterates throug the list of ingredients and displays the corresponding amount
	 */
	 
    void browse(void);

	
	/**
	 * @brief returns a specific ingredient name
	 * @return ingredient name
	 * @param  i - returns the ingredient at the position of the parameter
	 */
	 
	 
    std::string getZutat(int i);

	
	
	/**
	 * @brief returns the amount of available ingredients
	 *
	 * @return returns the size of the list of ingredients
	 */
    int getAnzahlVorhandeneZutaten();
    /**
     * @brief change ingredient amount
     * @param zutatID which ingredient
     * @param amount how much should be changed
     */
    void change_ingredient_amount(std::string zutatID,float amount);
    /**
     * @brief check if the ingredient is excisting in sufficient amount
     * @param s which ingredient
     * @param neededAmount for thr cocktail
     * @return bool if there is enough ingredient-amount
     */
    bool enough_ingredient_amount(std::string s,float neededAmount);
    /**
     * @brief show all recipes at the beginning without cutting out the ones not producable
     */
    void show_current_ingredients_amount();
	

private:
   // std::vector<std::string> * zutaten2;
    std::vector<Zutat>* zutaten;
    static const bool DEBUG = false;

    void zutatenEinlesen();
    //void DummyZutatenEinfuegen();

    virtual void ZutatenDateiEinlesen(std::string);

    int getZutatbyString(std::string s);
    void spezielleFaehigkeitenHinzufuegen();

    int anzahlDosierer;

};
static std::string FileName;
#endif
