//@(#) ZutatenVerwalter.cpp

#include "ZutatenVerwalter.h"
//

ZutatenVerwalter::ZutatenVerwalter(void) {

    this->zutaten = new std::vector<Zutat>;
    zutatenEinlesen();
    this->anzahlDosierer = zutaten->size();

}

ZutatenVerwalter::ZutatenVerwalter(const ZutatenVerwalter &z) {
    zutaten = new std::vector<Zutat>;
    for (unsigned int i = 0; i < z.zutaten->size(); i++) {
        Zutat temp = z.zutaten->at(i);
        this->zutaten->push_back(temp);
    }
    this->anzahlDosierer = z.zutaten->size();
}

ZutatenVerwalter::~ZutatenVerwalter(void) {

}

void ZutatenVerwalter::ZutatenDateiEinlesen(std::string myFile) {
    std::ifstream in;

    FileName = myFile;

    // Datei oeffnen
    in.open(FileName.c_str(), std::ios::in); // c_str wandelt den String in char[]
    // das braucht fstream

    if (!in) {// File konnte nicht geoeffnet werden
        std::string my_exception = "File not found: " + FileName;
        throw my_exception;
    }

    std::cout << "Oeffne Zutatendatei " << FileName << std::endl;

    /* Daten lesen und in Liste eintragen */
    std::string zeile;
    while (getline(in, zeile)) {

        // Cut trailing \r - to make Linux or Windows Files equal
        if (zeile.size() && zeile[zeile.size() - 1] == '\r') {
            zeile = zeile.substr(0, zeile.size() - 1);
        }


        bool found = false;


        for(unsigned int i=0 ; i<zutaten->size();i++){
            if(zutaten->at(i).getZutat_Name()==zeile){
                zutaten->at(i).addBehaelter();
                found = true;
                break;
            }

        }



        if(!found){
            Zutat temp;
            temp.set_Zutat_Name(zeile);
            this->zutaten->push_back(temp);
        }
    }





    /* Datei wieder schliessen */
    in.close();
}

/*
void ZutatenVerwalter::DummyZutatenEinfuegen() {
    zutaten->push_back("Limettenstuecke");
    zutaten->push_back("Zucker");
    zutaten->push_back("Cointreau");
    zutaten->push_back("Eis");
    zutaten->push_back("Wodka");
    zutaten->push_back("Rum weiss");
    zutaten->push_back("Zitronensaft");
    zutaten->push_back("Grenadine");
    //    zutaten->push_back("Limettensaft");
    //    zutaten->push_back("Tequilla");
    //    zutaten->push_back("Gin");
    //    zutaten->push_back("Noilly Prat");
}*/

void ZutatenVerwalter::zutatenEinlesen() {
    // int einlesen(list<string>* zutaten, std::string FileName)
    // Stream anlegen

    try {
        if (DEBUG) {
           // DummyZutatenEinfuegen();
        } else {

            //Working Directory auf den Projektordner setzen
            ZutatenDateiEinlesen("zutaten.txt");
        }

            this->browse(); // Eingelesene Zutaten ausgeben *********

        // Mischen und Stampfen, etc hinzufuegen
        spezielleFaehigkeitenHinzufuegen();
    } catch (std::string &exc) {
        std::cerr << exc << std::endl;
        exit(1);
    }

}

void ZutatenVerwalter::browse(void) {
    std::cout << "*********** Verfuegbare Einheiten bzw. Zutaten: ***********" << std::endl;
    for (unsigned int i = 0; i < zutaten->size(); i++/*std::string zutat : zutaten*/) {
        std::cout << zutaten->at(i).getZutat_Name() << std::endl;
    }
    std::cout << "**********************************************************" << std::endl;
}

void ZutatenVerwalter::spezielleFaehigkeitenHinzufuegen() {

    bool special = true;
    zutaten->push_back(Zutat("Mischen", special));
    zutaten->push_back(Zutat("Stampfen", special));
    zutaten->push_back(Zutat("Schuetteln", special));

}

std::string ZutatenVerwalter::getZutat(int i) {
   // return zutaten->at(i);
    return zutaten->at(i).getZutat_Name();
}

int ZutatenVerwalter::getAnzahlVorhandeneZutaten() {
    return zutaten->size();
}

void ZutatenVerwalter::change_ingredient_amount(std::string zutatID, float amount) {

    if(!zutaten->at(getZutatbyString(zutatID)).getIsSpecial()) {
        zutaten->at(getZutatbyString(zutatID)).alter_remaining_amount(amount);
    }
}

int ZutatenVerwalter::getZutatbyString(std::string s) {
    for(unsigned int i = 0; i < zutaten->size() ; i++){
        if(s == zutaten->at(i).getZutat_Name()) {
           return i;
        }
    }
    return -1;
}

bool ZutatenVerwalter::enough_ingredient_amount(std::string s,float neededAmount) {
       return zutaten->at(getZutatbyString(s)).enough_amount_for_recipe(neededAmount);
}

void ZutatenVerwalter::show_current_ingredients_amount() {
    using namespace std;
    for(unsigned int i = 0; i < zutaten->size(); i++){
        bool condition = zutaten->at(i).getIsSpecial();
        if(condition == false) {
            zutaten->at(i).printBehaelter();
        }

    }

}
