//@(#) MischbaresRezeptbuch.h

#ifndef MISCHBARESREZEPTBUCH_H_H
#define MISCHBARESREZEPTBUCH_H_H

#include "Rezept.h"
#include "Rezeptbuch.h"
#include "ZutatenVerwalter.h"

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib> // fuer exit() unter Linux

/**
 * @class MischbaresRezeptbuch
 *  Kennt die mischbaren Rezepte.
 *  Kann diese Ausgeben.
 *  @brief Buch der mischbaren Rezepte
 */
class MischbaresRezeptbuch : public Rezeptbuch {
public:
    /**
     * @brief constructor for MischbaresRezeptbuch
     * @param ze gets a Zutatenverwalter, so it knows which ingridients are available
     */
    explicit MischbaresRezeptbuch(ZutatenVerwalter * ze);
    /**
     * @brief goes through all the available recipes and prints how many and which they are
     */
    void browse();

    /**
     * @brief iterate through all recipes and mute if not valid
     * @return number of recipes
     */

    int RezepteAusblenden();

private:
    ZutatenVerwalter * myZutatenVerwalter;

    //std::vector<Rezept *> rezepte;
/**
 * @brief if a instruction is not mixable, delete it from possibilities
 */
    void muteRezept(int i);
    //void seitenRausreissen();
    void show_all_recipes();
    void setZutatenVerwalter(ZutatenVerwalter * ze);
    bool zutatValid(std::string zutaten_name,float menge);
    bool rezeptValid(int rezeptNummer);
};

#endif
