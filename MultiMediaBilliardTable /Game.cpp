
#include "Game.h"

Game::Game()
{

}

Game::Game(bool log_type)
{
	Logger::set_debug_lvl(log_type);
	Logger::log(DEBUG,"[Game] \n");
	
	_led_controller = new Led_Controller(NUMBER_OF_LEDS);
	_leds = _led_controller->get_leds();
	_eventPublisher = new EventPublisher();
	
	for (size_t i = 0; i < 6; i++)
	{
		std::string topic;
		switch (i)
		{
		case 0:  topic = "MultiMediaBilliardTable\\pockets\\LeftHead";
			break;
		case 1:  topic = "MultiMediaBilliardTable\\pockets\\LeftSide";
			break;
		case 2:	 topic = "MultiMediaBilliardTable\\pockets\\LeftFoot";
			break;
		case 3:	 topic = "MultiMediaBilliardTable\\pockets\\RightHead";
			break;
		case 4:  topic = "MultiMediaBilliardTable\\pockets\\RightSide";
			break;
		case 5:  topic = "MultiMediaBilliardTable\\pockets\\RightFoot";
			break;
		}
		Pocket *p = new Pocket(MQTT_HOST, 1883, topic, i, _eventPublisher, "Pocket", _led_controller);
		_pockets.push_back(p);
		p->connectToBroker();
	}
	
	_teams.push_back(new Team(MQTT_HOST, 1883, "MultiMediaBilliardTable\\logic\\teamNames\\team1"));
	_teams.push_back(new Team(MQTT_HOST, 1883, "MultiMediaBilliardTable\\logic\\teamNames\\team2"));
	_teams.at(0)->connectToBroker();
	_teams.at(1)->connectToBroker();
}

Game::~Game()
{
	std::cout << "[~Game]" << std::endl;
	delete _led_controller;
	delete _eventPublisher;

	for (size_t i = 0; i < _pockets.size(); i++)
	{
		delete _pockets.at(i);
	}

	for (size_t i = 0; i < _balls.size(); i++)
	{
		delete _balls.at(i);
	}
	delete _leds;
}


Game::Game(const Game &game)
{
	_led_controller = game.   _led_controller;
	_eventPublisher  = game.    _eventPublisher;
	_leds           = new std::vector<Led*>();

	for ( auto pocket : game._pockets )
	{
		_pockets.push_back( pocket );
	}

	for (int i = 0; i < NUMBER_OF_LEDS; i++ )
	{
		_leds->push_back(new Led);
	}
	
	for ( auto &team : game._teams )
	{
		_teams.push_back( team );
	}
	for ( auto element : game._balls_pocketed_this_turn )
	{
		_balls_pocketed_this_turn.push_back( element );
	}
	for ( auto ball: game._balls )
	{
		_balls.push_back( new Ball(*ball));
	}
}

Game & Game::operator=(const Game & game)
{
	_play_balls_start_amount          = game. _play_balls_start_amount;
	_play_balls_inplay_amount         = game._play_balls_inplay_amount;
	_amount_of_teams                = game.       _amount_of_teams;
	_incoming_team                  = game.         _incoming_team;
	_ball_hit_border                  = game.         _ball_hit_border;

	_first_turn                       = game.              _first_turn;
	_game_finished                    = game.           _game_finished;
	_game_idle                        = game.               _game_idle;
	_ball_legally_pocketed            = game.   _ball_legally_pocketed;
	_break_by_shutout                 = game.        _break_by_shutout;
	_free_foul_ball_placement         = game._free_foul_ball_placement;
	_first_collision_of_turn          = game. _first_collision_of_turn;
	_ball_hit_border                  = game.         _ball_hit_border;
	_foul_occurred_this_turn          = game. _foul_occurred_this_turn;
	_allow_start_of_turn              = game.     _allow_start_of_turn;


	_sleep_time_per_run_iteration     = game.    _sleep_time_per_run_iteration;
	_timer                            = game.                           _timer;
	_turns_team                     = game.                    _turns_team;
	_ticks_until_idle                 = game.                _ticks_until_idle;
	_ticks_since_last_ball_movement   = game.  _ticks_since_last_ball_movement;
	_min_idle_ticks                   = game.                  _min_idle_ticks;
	_ticks_ball_is_considered_missing = game._ticks_ball_is_considered_missing;
	_ticks_till_next_key_input        = game.       _ticks_till_next_key_input;
	_ticks_since_last_key_input       = game.      _ticks_since_last_key_input;



	/* load pocket DATA */
	for ( int i = 0; i < game._pockets.size(); i++ )
	{
		*_pockets.at(i) = *game._pockets.at(i);
	}
	/* load player DATA */
	for ( int i = 0; i < game._teams.size(); i++ )
	{
		*_teams.at(i) = *game._teams.at(i);
	}
	
	_balls_pocketed_this_turn.clear();
	for (auto element : game._balls_pocketed_this_turn)
	{
		_balls_pocketed_this_turn.push_back(element);
	}
	
	for (int i = 0; i < NUMBER_OF_LEDS; i++ )
	{
		(*(*_leds).at(i)) = (*game._leds->at(i));
	}
	
	
	std::vector<Ball*>::iterator it;
	bool found_match = false; 
	
	/* Check for missing ball objects and load the data of the game save state object */
	
	for(int i = 0; i < game._balls.size(); i++)
	{
		for(std::vector<Ball*>::iterator it = _balls.begin(); it != _balls.end(); ++it) 
		{
			if ( game._balls.at(i)->get_subNumber() == (*it)->get_subNumber()) 
			{
				(*it)->set_ball(*game._balls.at(i));
				found_match = true;
			}
		}
		if ( !found_match )
		{
			Ball* new_ball  = new Ball( *game._balls.at(i) );
			
		//	std::string topic = "MultimediaBilliardTable/BallPos/" + std::to_string(i);
		//	Ball *b = new Ball(MQTT_HOST, 1883, topic, i, _visualisation, "Ball");

			_balls.insert   ( _balls.begin() + i, new_ball );	
			new_ball->connectToBroker();
		}
		
		found_match = false;
		
	}
	
	/* delete all balls that are exisiting in the current game object but are not in the game save state object */
	
	for(std::vector<Ball*>::iterator it = _balls.begin(); it != _balls.end(); ) 
	{
		for (auto saved_ball : game._balls)
		{
			if ( saved_ball->get_subNumber() == (*it)->get_subNumber() ) 
			{
				++it;
				continue;
			}
		}
		
		if (it != _balls.end())
		{
			it = _balls.erase(it);
		}		
	}
	
	return *this;
}

void Game::check_for_updatedBalls()
{
	for (auto const& value : _balls) {
		if (value->get_posUpdatedFlag())
		{
			std::cout << "BALL UPDATED " << value->get_subNumber() << "\n";

			if (EDGE_X - abs(value->get_x()) < BALL_RADIUS || EDGE_Y - abs(value->get_y()) < BALL_RADIUS)
			{
				value->set_border_touched_this_turn(true);
			}
			_ticks_since_last_ball_movement = 0;
			detect_collision(value);
			value->set_posUdpdatedFlag(false);
		}
		if (value->get_ticks_ball_missing() > _ticks_ball_is_considered_missing)
		{
			//std::cout << "BALL MISSING : " << value->get_subNumber() << "\n";
		}
	}
}

void Game::check_pocketedBalls()
{
	for (auto const& value : _pockets) {
		if (value->get_recent_changes())
		{
			std::vector<int> temp = value->get_unprocessed_changes();
			for (auto const& ball : temp) {
				_balls_pocketed_this_turn.push_back(ball);
			}
		}
	}		
}

void Game::evaluate_pocketedBalls()
{

}

void Game::detect_collision(Ball* ball_ptr)
{
	for (auto const& value : _balls)
	{
		if (value != ball_ptr)
		{
			float ball_distance = sqrt(pow(value->get_x() - ball_ptr->get_x(),2) + pow(value->get_y() - ball_ptr->get_y(),2));
			//std::cout << ball_distance << std::endl;

			if (ball_distance < 2 * BALL_RADIUS)
			{
				// collision detected
				 std::cout << ball_distance << "COLLISION __>    " <<  value->get_subNumber() << " : " << ball_ptr->get_subNumber() << std::endl;
				
				_detected_collision.push_back({ value, ball_ptr });
			} 
		}
		else
		{

			if (2.0 - abs(value->get_x()) < BALL_RADIUS || 1.0 - abs(value->get_x()) < BALL_RADIUS)
			{
				value->set_border_touched_this_turn(true);
			}
			else
			{
				detect_collision_vector(value);
			}
			
		}
	}
	//evaluate_collisions();
}

void Game::detect_collision_vector(Ball* ball_ptr)
{

	ball_ptr->update_ball();
	if (abs(ball_ptr->get_prev_dir() - ball_ptr->get_dir()) > MOVEMENT_MARGIN)
	{
		std::cout << " Collision detected, ball " << ball_ptr->get_subNumber() << " has changed its course significantly. \n";
	}

}

void Game::evaluate_collisions()
{
	for (auto const& value : _detected_collision) {
		//std::cout << "fight     " << value[0]->get_subNumber() << "\n";

	}
}
void Game::validate_ball_positions() 
{

	_allow_start_of_turn = true;
	for (auto const& value : _balls) {
		if (value->get_subNumber() == 0)continue;

		float dif_x = abs(value->get_x() - value->get_start_x());
		float dif_y = abs(value->get_y() - value->get_start_y());		
		if (dif_x > 0.0572 / 2 || dif_y > 0.0572 / 2)
		{
			_allow_start_of_turn = false;
		}

	}

}

void Game::validate_play_ball_position()
{
	if (_first_turn)
	{
		if ((*_balls.begin())->get_x() > -1.0)
		{
			_allow_start_of_turn = false;
		}

		// ball needs to be in head field;
	}
	else if (_free_foul_ball_placement)
	{
		// ball can be placed anywhere
	}
	else
	{
		float dif_x = abs((*_balls.begin())->get_x() - (*_balls.begin())->get_start_x());
		float dif_y = abs((*_balls.begin())->get_y() - (*_balls.begin())->get_start_y());

		if (dif_x > 0.0572 / 2 || dif_y > 0.0572 / 2)
		{
			_allow_start_of_turn = false;
		}
	}
}

 void Game::change_turn() 
 {
 
 };
 void Game::new_turn() 
 {
	// reset all border_touched flaggs from all balls in play
 };




void Game::run()
{
	check_for_updatedBalls();
	

	if (_game_finished)
	{

	}


	if (_game_idle)
	{
		if (_ticks_since_last_ball_movement <= 0)
		{

		}
		std::cout << " idle : " << _game_idle << "\n";
	}
	else 
	{
		if (_ticks_since_last_ball_movement <= _ticks_until_idle)
		{
			std::cout << "running  idle : " << _game_idle << "\n";
		} 
		else
		{
			std::cout << "stop turn idle : " << _game_idle << "\n";
		}
	}
		
	
	
		


	_timer++;
	

	_sleep(_sleep_time_per_run_iteration);
}
