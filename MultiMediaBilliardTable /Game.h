
#pragma once


#ifndef MULTIMEDIABILLIARDTABLE_1_GAME_H
#define MULTIMEDIABILLIARDTABLE_1_GAME_H


#define NUMBER_OF_LEDS 360
#define BALL_RADIUS 0.1
#define EDGE_X 2.0
#define EDGE_Y 1.0
#define MOVEMENT_MARGIN 4


#include <vector>
#include <thread>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <cmath>
#include <algorithm>

#include "Starter.h"
#include "Led_Controller.h"
#include "Ball.h"
#include "Pocket.h"
#include "EventPublisher.h"
#include "Team.h"
#include "Logger.h"

/*
@class Game
@brief Game is the class for realizing a Game
*/

enum GAME_EVENTS {
	FOUL = 0

};


class Game
{
public:
	/*
	@brief the constructor of the class Game
	*/
	Game();
	/*
	@param log_type is the param for debug/release..
	@brief the parameterized constructor of the class Game with a logger parameter

	In this parameterized constructor a new Led_Controller and a new Visualization will be created.
	Also creates new Pockets and push them into a vector. The amount of player, which
	was choosen before, will be created.
	*/
	Game(bool log_type);
	/*
	@brief the desctructor of the class Game
	*/
	~Game();
	/*
	@param game is the reference to an object of the same type
	@brief the copy constructor of the class Game

	In this copy constructor a copy of the object game will be created

	*/
	Game(const Game &game);
	/*
	@param game is the reference to an object of the same type
	@brief an overload operator =

	this method is an overload operator = which gets a reference of an object and
	assigns the variables of this object to the variables in this class. 
	It also loads the pocket DATA, player DATA and led Data into the vectors from 
	the refence object to the vectors in this class. The pocketes balls will be cleared.
	It checks if there is a ball missing and if a missing ball was found, it creates a
	new ball objects and pushes it to the vector for balls. Delete all balls that are
	exisiting in the current game object but are not in the game save state object.

	*/
	Game& operator= (const Game& game);
	/*
	@brief checks the state of the game

	if the game is finished, nothing happens. If the game is in state idle, there will be
	a message which idle. Else it will message if u have enough ticks the running idle,
	or stop turn idle.
	*/
	virtual void run();
	/*
	@ brief not implemented
	*/
	virtual void run_manual() {}
	std::vector<Led*>* _leds; 
protected:

/**
	centralized management components
*/
	EventPublisher*  _eventPublisher;
	Led_Controller* _led_controller; 
	
/**
	networking related game components
*/
	std::vector<Ball*>   _balls;  
	std::vector<Pocket*> _pockets;  
/**
	methods changing game state that need to be overloaded by the specific game modes
*/
	/*
	@brief not implemented
	*/
    virtual void  setup(){}
	/*
	@brief not implemented
	*/
	virtual void  new_turn();
	/*
	@brief not implemented
	*/
	virtual void  change_turn();
	/*
	@brief not implemented
	*/
	virtual void  win_and_clean_up(){}
	/*
	@brief not impelmented
	*/
    virtual void  set_ball_start_positions(){}
	
	/*
	@brief checks for pocketed balls
	*/
	void check_pocketedBalls();
	/*
	@brief check for updated balls

	if a posupdatedflag is true and the ball was touched then change border touched this
	turn to true. Also change the ticks when the ball was moved to zero and give the value
	of the ball to detected_collision() and change the posupdatedflag to false.
	*/
	void check_for_updatedBalls();	
	/*
	@brief checks the position of all balls

	checks the position of all balls and allows of forbids start_of_turn if the
	ball with the subNumber 0 (white ball) is on his last place.
	*/
	void validate_ball_positions();
	/*
	@brief checks if you are allowed to play

	if its the first turn, the ball has to be in head field, else after a foul, 
	it can be placed anywhere or it has to be on his last seen place.
	*/
	void validate_play_ball_position();
	/*
	@brief not implemented
	*/
	virtual void evaluate_collisions();
	/*
	@brief not implemented
	*/
	virtual void evaluate_pocketedBalls();
	/*
	@brief checks for collision
	@ball_ptr a ball which moved

	checks every ball for collision with another ball. Checks if this ball had a collsion
	with another ball.
	*/
	void detect_collision(Ball* ball_ptr);
	/*
	@brief sends a message if the ball changed its course
	*/
	void detect_collision_vector(Ball* ball_ptr);
	
	
/**
	basic game attributes shared by all game modes

	//top_C/ means that this attribute is only needed it the top cameras are included in the system 
*/
	int   _play_balls_start_amount   =  0;
	int   _play_balls_inplay_amount  =  0;
	int   _amount_of_teams         =  2;
	int   _incoming_team           = -1;
	int   _ball_hit_border           =  0; //top_C/


	
	bool  _game_idle                 =  true; //top_C/
	bool  _first_turn                =  true; //top_C/
	bool  _game_finished             = false;
	bool  _break_by_shutout          = false; 
	bool  _allow_start_of_turn       = false; //top_C/
	bool  _ball_legally_pocketed     = false;
	bool  _first_collision_of_turn   =  true; //top_C/
	bool  _foul_occurred_this_turn   = false;
	bool  _free_foul_ball_placement  =  true; //top_C/
	
	int   _sleep_time_per_run_iteration     = 100;
	int   _timer                            =   0;
	int   _turns_team                     =   0;
	int	  _ticks_until_idle                 =  30; //top_C/
	int   _min_idle_ticks                   =  30; //top_C/
	int   _ticks_ball_is_considered_missing =  50; //top_C/
	int	  _ticks_since_last_ball_movement   = _ticks_until_idle + 1; //top_C/ 
	int   _ticks_till_next_key_input        =   5;
	int   _ticks_since_last_key_input       =   0;



	std::vector<Team*>               _teams; 
	std::vector<  std::vector<Ball*> > _detected_collision; //top_C/
	std::vector<int>                   _end_of_turn_events; /* include if this gets a relevant purpose */
	std::vector<int>                   _balls_pocketed_this_turn;

	Game* _prev_game = NULL;
};


#endif