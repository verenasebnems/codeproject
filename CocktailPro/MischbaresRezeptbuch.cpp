//@(#) MischbaresRezeptbuch.cpp


#include "MischbaresRezeptbuch.h"
#include <iostream>
//

void MischbaresRezeptbuch::browse() {
    //this->seitenRausreissen();
    std::vector<Rezept> mutedRecepies;
    std::vector<int> recipeId;
    std::cout << "*********************************************" << std::endl;

    std::cout << "Es gibt mischbare " << this->getAnzahlRezepte() - this->RezepteAusblenden() << " Cocktails" << std::endl;
    //this->seitenRausreissen();
    for (int i = 0; i<this->getAnzahlRezepte(); i++) {
        Rezept* r = this->getRezept(i);
        if(!r->getMuted()) {
            std::cout << i + 1 << ". ";
            r->browse();
            std::cout << std::endl;
        }
        else
        {
          mutedRecepies.push_back(*r);
          recipeId.push_back(i +1);
        }

    }
    std::cout << "*********************************************" << std::endl;
    std::cout <<"----- nicht mischbare Cocktails ------ \n";
    for(unsigned int i = 0; i < mutedRecepies.size(); i++)
    {
        std::cout << recipeId.at(i) << ". ";
        mutedRecepies.at(i).browse();
        std::cout << std::endl;
    }
    std::cout << "*********************************************" << std::endl;



}

void MischbaresRezeptbuch::show_all_recipes() {
    std::cout << "*********************************************" << std::endl;
    std::cout << "Es gibt " << this->getAnzahlRezepte() << " Cocktails" << std::endl;
    //this->seitenRausreissen();
    for (int i = 0; i<this->getAnzahlRezepte(); i++) {
        Rezept* r = this->getRezept(i);
        std::cout << i + 1 << ". ";
        r->browse();
        std::cout << std::endl;
    }
    std::cout << "*********************************************" << std::endl;

}

MischbaresRezeptbuch::MischbaresRezeptbuch(ZutatenVerwalter * zv) {
    setZutatenVerwalter(zv);
    // Debug *********
    std::cout << "********** Rezepte vor dem Filtern **********" << std::endl;

    // ******************


   this->show_all_recipes();

}

void MischbaresRezeptbuch::setZutatenVerwalter(ZutatenVerwalter * zv) {
    myZutatenVerwalter = zv;
}

/*void MischbaresRezeptbuch::seitenRausreissen() {

    // itarates and validates all Rezepte

    for (int i = this->getAnzahlRezepte()-1; i>=0; i--) {
        if(!rezeptValid(i))
        {
            deleteRezept(i);
        }
    }
}*/

bool MischbaresRezeptbuch::zutatValid(std::string zutaten_name,float menge){
    bool ZutatOK = false;


    for (int k = 0; k < myZutatenVerwalter->getAnzahlVorhandeneZutaten(); k++) {
        if (myZutatenVerwalter->getZutat(k) == zutaten_name && myZutatenVerwalter->enough_ingredient_amount(zutaten_name,menge)) {
            ZutatOK = true;
            break;
        }
    }
    return ZutatOK;
}

bool MischbaresRezeptbuch::rezeptValid(int rezeptNummer){
    bool rezeptOK;
    Rezept* r = getRezept(rezeptNummer);
    rezeptOK = true;

    //iterates and validates all ingredients of the given Rezept number

    for (int j = 0; j < r->getAnzahlRezeptschritte(); j++) {

        if(!zutatValid(r->getRezeptSchritt(j)->getZutat(),r->getRezeptSchritt(j)->getMenge())){
            rezeptOK = false;

            break;
        }

    }
    return rezeptOK;

}

int MischbaresRezeptbuch::RezepteAusblenden() {

    int retcnt = 0;

    for (int i = this->getAnzahlRezepte()-1; i>=0; i--) {
        if(!rezeptValid(i))
        {
            muteRezept(i);
            retcnt++;
        }
    }

    return retcnt;
}

void MischbaresRezeptbuch::muteRezept(int i) {

    if(validRezept(i))
    {
        std::list<Rezept*>::iterator p; // Iterator
        advance(p = m_Rezepte.begin(), i); // now p points to the i-th Element
        (*p)->setMuted(true);

    }

}