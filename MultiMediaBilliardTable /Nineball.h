#pragma once

#include "Game.h"
#include <conio.h>
#include <Windows.h>

/**
* @class Nineball
* 
* @brief This class is a game of Nineball.
* 
* Inherits from Game and contains the functions and variables needed
* to play a game of Nineball such as changing turns, evaluate collisions and winning.
*/
class Nineball : public Game
{
	public:
		/**
		* @brief This standard constructor creates an instance of Nineball.
		* 
		* @return pointer to the created object instance
		*/
		Nineball();

		/**
		* @brief This constructor creates an instance of Nineball and sets up the game.
		* 
		* @param [in] log_type A boolean, that determines whether the game should have logging enabled.
		* 
		* @return pointer to the created object instance
		*/
		Nineball(bool log_type);

		/**
		* @brief This constructor creates an instance of Nineball and applies the values of some variables of the parameter to this games variables.
		* 
		* @param [in] orig A Nineball game with important variable values that need to be copied to the current game.
		* 
		* @return pointer to the created object instance
		*/
		Nineball(const Nineball &orig);

		/**
		* @brief Overloads the = operator to assign Nineball object instances.
		*/
		Nineball& operator= (const Nineball &orig);

		/**
		* @brief Destructor for Nineball.
		*/
		~Nineball();

	private:
		/**
		* @brief Checks/Validates balls, pockets and game state, and continues from there.
		*/
		void run();

		/**
		* @brief Checks which balls collided and might declare a foul.
		*/
		void evaluate_collisions();

		/**
		* @brief Checks which balls were pocketed this turn and might declare a foul.
		*/
		void evaluate_pocketedBalls();
		
		/**
		* @brief (Re)Sets a bunch of variables required to start a new turn.
		*/
		void new_turn();

		/**
		* @brief Changes the incomming player and starts some visualizations.
		*/
		void change_turn();

		/**
		* @brief Starts some visualizations.
		*/
		void win_and_clean_up();

		/**
		* @brief Sets the start positions for all balls.
		*/
		void set_ball_start_positions();

		/**
		* @brief Creates a new ball and connects it to the broker.
		*/
		void spot_nineball();


		/**
		* An iterator indicating the ball that needs to be contacted by the cue ball.
		*/
		std::vector<Ball*>::iterator _target_ball;

		/**
		* An integer number of the ball that needs to be pocketed in order to win the game (default 9).
		*/
		int  _winning_ball = 9;

		/**
		* A boolean that indicates whether the winning ball as been pocketed (default false).
		*/
		bool _winning_ball_pocketed = false;
	
		/**
		* The previous game of Nineball (default NULL).
		*/
		Nineball* _prev_nine_game = NULL;
};
